from nameko.rpc import rpc
from nameko.standalone.rpc import ClusterRpcProxy


class ServiceA:
    name = "servicea"
    config = {
        "AMQP_URI": "amqp://guest:guest@localhost:5672"
    }


    @rpc
    def hello(self, name):
        print("Service A here...")

        with ClusterRpcProxy(self.config) as cluster_rpc:
            greet = cluster_rpc.serviceb.hello.call_async(self.name)
            return "{0} Oh hai {1}, - Service A.".format(greet.result(), name)
